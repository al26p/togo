
# To-Go

## Description

A "simple" project to create a To-Do list using a Go Backend. 

A React Frontend should be developed later.

## Installation

All the soft is using (for the moment). The easy path is to simply apply ```docker compose up``` (requires docker installed on your system). 

If you have a working installation of Go you can simply run ```go mod tidy``` then ```go run .```. A PostgreSql is required. 

## Support

This piece of software is under development and the current status is more WIP than alpha. THings are broken, don't make PR/Issues. I know. 

But if you have some feedbacks to help me write a better/cleaner code, don't hesitate.

## Roadmap

The current status is very WIP. 

Things I'm working on:
- [x] Dockerize the app
- [x] Connect the DB
- [x] Begin of a data struture
- [x] Expose an API
- [ ] Define a correct data structure
- [ ] Code data access methods
- [ ] Build a decent API
- [ ] Implement tests

Next steps : 
- [ ] Create a Helm/other template to deploy on a K8s kluster
- [ ] Add authentication 
- [ ] Todos can be assigned to another user/group

## Contributing

For the moment the project is not open to contributions. 

## Authors and acknowledgment
Todo : Cite all the opensource soft used... 

## License
DWYW

## Project status
It's only the beginning. It should be a rather small project. I'm actually developing this piece of software as a training for further work/evaluation/interviews. 
But if it goes well, I'm looking for an alternative to MS ToDo so... Why not coding it ;) 
***

## Test and deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

