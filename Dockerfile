# syntax=docker/dockerfile:1

FROM golang:1.18-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

# Download necessary Go modules
RUN go mod download

# Copy app
COPY main.go ./
COPY main_test.go ./
COPY model ./model
COPY orm ./orm

# Build
RUN go build -o /docker-api-project

# Run
CMD [ "/docker-api-project" ]