package orm

import (
	"api_project/model"
	"gorm.io/gorm"
	"log"
)

func CreateUser(db *gorm.DB, s string) uint {
	u := model.Person{Username: s}
	result := db.Omit("GroupID").Create(&u)
	if result.Error != nil {
		log.Println(result.Error)
		return 0
	}
	db.Save(&u)
	return u.ID
}

func CreateGroup(db *gorm.DB, s string) uint {
	u := model.Group{Name: s}
	result := db.Create(&u)
	if result.Error != nil {
		log.Println(result.Error)
		return 0
	}
	db.Save(&u)
	return u.ID
}

func AddUserToGroup() {
	//TODO
	panic("Implement me !")
}

func AddTodo() {
	//TODO
	panic("Implement me !")
}

func GetTodo() {
	//TODO
	panic("Implement me !")
}

/*
Use variadic functions like this
func addPerson(args ...interface{}) error {
    if len(args) > 3 {
        return fmt.Errorf("Wront number of arguments passed")
    }
    p := &person{}
    //0 is name
    //1 is gender
    //2 is age
    for i, arg := range args {
        switch i {
        case 0: // name
            name, ok := arg.(string)
            if !ok {
                return fmt.Errorf("Name is not passed as string")
            }
            p.name = name
        case 1:
            gender, ok := arg.(string)
            if !ok {
                return fmt.Errorf("Gender is not passed as string")
            }
            p.gender = gender
        case 2:
            age, ok := arg.(int)
            if !ok {
                return fmt.Errorf("Age is not passed as int")
            }
            p.age = age
        default:
            return fmt.Errorf("Wrong parametes passed")
        }
    }
    fmt.Printf("Person struct is %+v\n", p)
    return nil
}
*/
