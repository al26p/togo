package main

import (
	"api_project/model"
	"fmt"
	"github.com/kataras/iris/v12"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

func main() {

	// Create the databse connection
	dsn := "host=db port=5432 user=postgres dbname=postgres password=mysecretpassword sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn))
	// End the program with an error if it could not connect to the database
	if err != nil {
		panic("could not connect to database")
	}

	// Create the default database schema and a table for the orders
	err = db.AutoMigrate(&model.Item{}, &model.Group{}, &model.Person{})
	if err != nil {
		log.Println(err)
		return
	}

	// Run the function to create the new iris App
	app := newApp(db)
	someData(db)
	// Start the web server on port 8080
	app.Run(iris.Addr(":8080"), iris.WithoutServerError(iris.ErrServerClosed))

}

func someData(db *gorm.DB) {
	/*g := model.Group{
		Name: "Main",
		People: []model.Person{model.Person{
			Username: "Test",
		}},
	}
	fmt.Println(db.Create(&g))
	db.Save(&g)*/
	/*p := model.Person{Username: "Admin"}
	fmt.Println(db.Omit("GroupID").Create(&p))
	g := model.Group{}
	db.First(&g)
	g.People = append(g.People, p)
	db.Save(&g)*/
	/*t := model.Item{
		Title:       "Ceci est un test",
		State:       false,
		DueDate:     time.Now().Add(24 * time.Hour),
		Labels:      "test,init",
		Description: "Waaaaaa le premier todo !",
		OwnerID:     1,
		OwnerType:   "group",
	}
	fmt.Println(db.Create(&t))*/
	var groups []model.Group
	var users []model.Person
	db.Model(&model.Group{}).Association("People").Find(&users)
	fmt.Println(users, groups)
	var item = model.Item{}
	db.First(&item)
	fmt.Println(item)
	//fmt.Println("Created group", orm.CreateGroup(db, "moi"))
}

func newApp(db *gorm.DB) *iris.Application {
	// Initialize a new iris App
	app := iris.New()

	// Register the request handler for the endpoint "/"
	app.Get("/", func(ctx iris.Context) {
		// Return something by adding it to the context
		ctx.Text("Hello World")
	})

	// Register an endpoint with a variable
	app.Get("{name:string}", func(ctx iris.Context) {
		_, _ = ctx.Text(fmt.Sprintf("Hello %s", ctx.Params().Get("name")))
	})

	// Define the slice for the result
	var orders []model.Item

	// Endpoint to perform the database request
	app.Get("/orders", func(ctx iris.Context) {
		db.Find(&orders)
		_ = ctx.JSON(orders)
	})

	return app
}
