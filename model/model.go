package model

import (
	"fmt"
	"gorm.io/gorm"
	"time"
)

// Item is the database representation of an order
type Item struct {
	gorm.Model
	Title       string
	State       bool
	DueDate     time.Time
	Labels      string `gorm:"type:varchar"`
	Description string `gorm:"type:varchar"`
	OwnerID     int    `gorm:"foreignKey"`
	OwnerType   string
}

func (i Item) String() string {
	return fmt.Sprintf("To-Do ID : %v | %v\n Done : %v Labels : %v\n %v \n %v %v", i.ID, i.Title, i.State, i.Labels, i.Description, i.OwnerType, i.OwnerID)
}

type Person struct {
	gorm.Model
	Username string `gorm:"type:varchar(45)"`
	Items    []Item `gorm:"polymorphic:Owner;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	GroupID  uint
}

func (p Person) String() string {
	return fmt.Sprintf("Person ID : %v | %v \n Group : %v\n Todos :%v", p.ID, p.Username, p.GroupID, p.Items)
}

type Group struct {
	gorm.Model
	Name   string `gorm:"type:varchar(45)" `
	Items  []Item `gorm:"polymorphic:Owner;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	People []Person
}

func (g Group) String() string {
	return fmt.Sprintf("Group ID : %v | %v\n Members : %v\n Todos : %v", g.ID, g.Name, g.People, g.Items)
}
